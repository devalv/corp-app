# tessa-app

> a project by nidointeractive

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn start

# build for production with minification
yarn build

# deploy to firebase
yarn deploy
```
