require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')
require('cors')({origin: true})
import Vue from 'vue'
import Vuetify from 'vuetify'
import * as firebase from 'firebase'
import VueVideoPlayer from 'vue-video-player'
import '../node_modules/vuetify/dist/vuetify.min.css'
import AlertCmp from './components/Shared/Alert.vue'
import App from './App'
import { store } from './store'
import router from './router'
import AsyncComputed from 'vue-async-computed'
/* import VueCamera from './components/shared/vue-camera.vue'
Vue.component('camera-component', VueCamera) */
Vue.use(AsyncComputed)
Vue.use(require('vue-moment'))
Vue.component('app-alert', AlertCmp)
Vue.use(VueVideoPlayer)
Vue.use(Vuetify, {
  theme: {
    primary: '#ff0000',
    secondary: '#cccccc',
    accent: '#8c9eff',
    error: '#b71c1c'
  }
})
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyDhbmoXkLZfXbs-GBdWytTvAe7uUrYNQTw',
      authDomain: 'corpappclients.firebaseapp.com',
      databaseURL: 'https://corpappclients.firebaseio.com',
      projectId: 'corpappclients',
      storageBucket: 'corpappclients.appspot.com',
      messagingSenderId: '754349739305'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('autoUsername', user)
        this.$store.dispatch('loadCustomers')
      }
    })
  }
})
