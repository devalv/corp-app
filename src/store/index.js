import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    superuser: false,
    loading: false,
    error: null,
    newCustomer: null,
    newUsers: null,
    username: null,
    allCustomers: null,
    allUsers: null
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
      if (payload !== null) {
        if (payload.id === 'kXFQl5kIxbbh3HvR2p0O5NFRmtb2' || payload.id === 'XjcCJlOV9Tf1cDIUbGMLPgBIpHu2') {
          state.superuser = true
        } else {
          state.superuser = false
        }
      }
    },
    setNewUsers (state, payload) {
      state.newUsers = payload
    },
    setAllCustomers (state, payload) {
      state.allCustomers = payload
    },
    setAllUsers (state, payload) {
      state.allUsers = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    clearError (state) {
      state.error = null
    },
    setNewCustomer (state, payload) {
      state.newCustomer = payload
    },
    setUsername (state, payload) {
      state.username = payload
    }
  },
  actions: {
    signUserUp ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              id: user.uid
            }
            commit('setUser', newUser)
            let addNewUser = {
              [payload.username]: {
                email: payload.email,
                uid: user.uid,
                username: payload.username,
                enabled: true
              }
            }
            firebase.database().ref('users').update(addNewUser)
            .then((data) => {
              console.log('user created')
              commit('setLoading', false)
            })
            .catch((error) => {
              console.log(error)
              commit('setLoading', false)
            })
          })
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
            console.log(error)
          })
    },
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            firebase.database().ref('users').once('value').then((data) => {
              Object.values(data.val()).map((element) => {
                if (element.uid === user.uid) {
                  if (element.enabled === true) {
                    commit('setUsername', element.username)
                    let newUser = {
                      id: user.uid
                    }
                    commit('setUser', newUser)
                  } else if (element.enabled === false) {
                    firebase.auth().signOut()
                    commit('setUser', null)
                    commit('setUsername', null)
                    commit('setError', 'El usuario está desactivado.')
                    throw new TypeError('User is disabled', '')
                  }
                }
              })
            })
            commit('setLoading', false)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
            console.log(error)
          }
        )
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {id: payload.uid})
    },
    autoUsername ({commit}, payload) {
      firebase.database().ref('users').once('value').then((data) => {
        Object.values(data.val()).map((element) => {
          if (element.uid === payload.uid) {
            commit('setUsername', element.username)
          }
        })
      })
    },
    logout ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
      commit('setUsername', null)
    },
    clearError ({commit}) {
      commit('clearError')
    },
    loadCustomers ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('customers').once('value')
      .then((data) => {
        let customers = []
        let obj = data.val()
        for (let key in obj) {
          customers.push({
            key: key,
            companyName: obj[key]['Company Name'] ? obj[key]['Company Name'] : '',
            contactPerson: obj[key]['Contact Person'] ? obj[key]['Contact Person'] : '',
            email: obj[key]['Email'] ? obj[key]['Email'] : '',
            phone: obj[key]['Phone'] ? obj[key]['Phone'] : '',
            country: obj[key]['Country'] ? obj[key]['Country'] : '',
            market: obj[key]['Market'] ? obj[key]['Market'] : '',
            enabled: obj[key]['Market'] ? obj[key]['Enabled'] : '',
            city: obj[key]['City'] ? obj[key]['City'] : '',
            truckCompany: obj[key]['Truck Company'] ? obj[key]['Truck Company'] : '',
            truckAccount: obj[key]['Truck Account'] ? obj[key]['Truck Account'] : '',
            truckDays: obj[key]['Truck Days'] ? obj[key]['Truck Days'] : '',
            cargo: obj[key]['Cargo Agency'] ? obj[key]['Cargo Agency'] : '',
            volume: obj[key]['Volume'] ? obj[key]['Volume'] : '',
            mainLength: obj[key]['Main Length'] ? obj[key]['Main Length'] : '',
            currentSuppliers: obj[key]['Current Suppliers'] ? obj[key]['Current Suppliers'] : '',
            status: obj[key]['Status'] ? obj[key]['Status'] : '',
            id: obj[key]['Id'] ? obj[key]['Id'] : '',
            notes: obj[key]['Notes'] ? obj[key]['Notes'] : '',
            createdBy: obj[key]['Created By'] ? obj[key]['Created By'] : '',
            createdOn: obj[key]['Created On'] ? obj[key]['Created On'] : '',
            modifiedBy: obj[key]['Modified By'] ? obj[key]['Modified By'] : '',
            modifiedOn: obj[key]['Modified On'] ? obj[key]['Modified On'] : '',
            handwritten: obj[key]['HandNotes'] ? obj[key]['HandNotes'].toString() : '',
            pictures: obj[key]['Pictures'] ? obj[key]['Pictures'].toString() : ''
          })
        }
        commit('setAllCustomers', customers)
        commit('setLoading', false)
      })
      .catch(
        (error) => {
          console.log(error)
          commit('setLoading', false)
        }
      )
    },
    loadUsers ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('users').once('value')
      .then((data) => {
        let users = []
        let obj = data.val()
        for (let key in obj) {
          users.push({
            key: key,
            email: obj[key]['email'] ? obj[key]['email'] : '',
            enabled: obj[key]['enabled'] ? obj[key]['enabled'] : false,
            uid: obj[key]['uid'] ? obj[key]['uid'] : '',
            username: obj[key]['username'] ? obj[key]['username'] : ''
          })
        }
        commit('setAllUsers', users)
        commit('setLoading', false)
      })
      .catch(
        (error) => {
          console.log(error)
          commit('setLoading', false)
        }
      )
    },
    saveTempPicture ({commit}, payload) {
      commit('clearError')
      commit('setLoading', true)
      var storageRef = firebase.storage().ref().child('pictures').child(payload.username + '/' + new Date())
      return storageRef.put(payload.blob)
      .then(function (snapshot) {
        console.log('Uploaded', snapshot.metadata.downloadURLs[0])
        commit('setLoading', false)
        return snapshot.metadata.downloadURLs[0]
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    },
    saveTempPictureString ({commit}, payload) {
      console.log('payload: ', payload.blob)
      commit('clearError')
      commit('setLoading', true)
      var storageRef = firebase.storage().ref().child('pictures').child(payload.username + '/' + new Date())
      return storageRef.putString(payload.blob, 'base64')
      .then(function (snapshot) {
        console.log('Uploaded', snapshot.metadata.downloadURLs[0])
        commit('setLoading', false)
        return snapshot.metadata.downloadURLs[0]
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    },
    saveTempNote ({commit}, payload) {
      commit('clearError')
      commit('setLoading', true)
      var storageRef = firebase.storage().ref().child('handnotes').child(payload.username + '/' + new Date())
      return storageRef.put(payload.blob)
      .then(function (snapshot) {
        console.log('Uploaded', snapshot.metadata.downloadURLs[0])
        commit('setLoading', false)
        return snapshot.metadata.downloadURLs[0]
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    },
    editCustomerStatus ({commit}) {
      commit('setLoading', true)
      commit('clearError')
      let ref = firebase.database().ref('customers')
      ref.on('value', function (snapshot) {
        let data = snapshot.toJSON()
        Object.entries(data).map((element) => {
          if (element[1].Status === '1') {
            element[1].Status = '1 - VISIT'
          }
          if (element[1].Status === '2') {
            element[1].Status = '2 - SHOW'
          }
          if (element[1].Status === '3') {
            element[1].Status = '3 - REFERENCE'
          }
          ref.child(element[0]).set(element[1])
        })
      }, function (errorObject) {
        console.log('The read failed: ' + errorObject.code)
      })
    },
    editCustomer ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      let client = {
        'Company Name': payload.companyName,
        'Contact Person': payload.contactPerson,
        'Email': payload.email,
        'Phone': payload.phone,
        'Market': payload.market,
        'Enabled': true,
        'Country': payload.country,
        'City': payload.city,
        'Truck Company': payload.truckCompany,
        'Truck Account': payload.truckAccount,
        'Truck Days': payload.truckDays,
        'Cargo Agency': payload.cargo,
        'Volume': payload.volume,
        'Main Length': payload.mainLength,
        'Current Suppliers': payload.currentSuppliers,
        'Status': payload.status,
        'Id': payload.id,
        'Notes': payload.notes,
        'Modified By': payload.modifiedBy,
        'Modified On': payload.modifiedOn,
        'Pictures': payload.pictures,
        'HandNotes': payload.handwritten
      }
      firebase.database().ref('customers/' + payload.key).update(client)
      .then((data) => {
        commit('setNewCustomer', client)
        commit('setLoading', false)
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    },
    deleteCustomer ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.database().ref('customers/' + payload).set(null)
      .then((data) => {
        commit('setLoading', false)
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    },
    updateUsers ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      let users = {}
      payload.map(element => {
        var key = element.key
        if (element.enabled === '') {
          element.enabled = false
        }
        users[key] = {
          email: element.email,
          enabled: element.enabled,
          uid: element.uid,
          username: element.username
        }
      })
      firebase.database().ref('users').set(users)
      .then((data) => {
        commit('setNewUsers', users)
        commit('setLoading', false)
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    },
    newCustomer ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      let client = {
        'Company Name': payload.companyName,
        'Contact Person': payload.contactPerson,
        'Email': payload.email,
        'Phone': payload.phone,
        'Market': payload.market,
        'Enabled': true,
        'Country': payload.country,
        'City': payload.city,
        'Truck Company': payload.truckCompany,
        'Truck Account': payload.truckAccount,
        'Truck Days': payload.truckDays,
        'Cargo Agency': payload.cargo,
        'Volume': payload.volume,
        'Main Length': payload.mainLength,
        'Current Suppliers': payload.currentSuppliers,
        'Status': payload.status,
        'Id': payload.id,
        'Notes': payload.notes,
        'Created On': payload.createdOn,
        'Created By': payload.createdBy,
        'Modified By': payload.modifiedBy,
        'Modified On': payload.modifiedOn,
        'Pictures': payload.pictures,
        'HandNotes': payload.handWritten
      }
      console.log('about to save', client)
      firebase.database().ref('customers').push(client)
      .then((data) => {
        console.log('client created', data)
        commit('setNewCustomer', client)
        commit('setLoading', false)
      })
      .catch((error) => {
        console.log('Error', error)
        commit('setLoading', false)
      })
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    username (state) {
      return state.username
    },
    superuser (state) {
      return state.superuser
    },
    loading (state) {
      return state.loading
    },
    error (state) {
      return state.error
    },
    allCustomers (state) {
      return state.allCustomers
    },
    allUsers (state) {
      return state.allUsers
    }
  }
})
