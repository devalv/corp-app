import {store} from '../store'

export default (to, from, next) => {
  if (store.getters.superuser) {
    next()
  } else {
    next('/signin')
  }
}
