import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import CompanyMain from '@/components/company/CompanyMain'
import VideoPlayer from '@/components/company/VideoPlayer'
import CustomersMain from '@/components/customers/CustomersMain'
import CustomerList from '@/components/customers/CustomerList'
import CustomerNew from '@/components/customers/CustomerNew'
import CustomerView from '@/components/customers/CustomerView'
import CustomerEdit from '@/components/customers/CustomerEdit'
import Profile from '@/components/user/Profile'
import UserList from '@/components/user/UserList'
import SignIn from '@/components/user/SignIn'
import NewUser from '@/components/user/NewUser'
import CameraView from '@/components/utilities/CameraView'
import AuthGuard from './auth-guard'
import AuthSuperUser from './auth-super-user'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/company',
      name: 'CompanyMain',
      component: CompanyMain
    },
    {
      path: '/video',
      name: 'VideoPlayer',
      component: VideoPlayer,
      beforeEnter: AuthGuard
    },
    {
      path: '/customers',
      name: 'CustomersMain',
      component: CustomersMain,
      beforeEnter: AuthGuard
    },
    {
      path: '/customersedit',
      name: 'CustomerEdit',
      component: CustomerEdit,
      beforeEnter: AuthGuard,
      props: true
    },
    {
      path: '/customerview',
      name: 'CustomerView',
      component: CustomerView,
      beforeEnter: AuthGuard,
      props: true
    },
    {
      path: '/customerlist',
      name: 'CustomerList',
      component: CustomerList,
      beforeEnter: AuthGuard
    },
    {
      path: '/customernew',
      name: 'CustomerNew',
      component: CustomerNew,
      beforeEnter: AuthGuard
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/camera',
      name: 'camera',
      component: CameraView,
      beforeEnter: AuthGuard
    },
    {
      path: '/newuser',
      name: 'newuser',
      component: NewUser,
      beforeEnter: AuthSuperUser
    },
    {
      path: '/userlist',
      name: 'userlist',
      component: UserList,
      beforeEnter: AuthSuperUser
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  mode: 'history'
})
